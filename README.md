Java [ELO][1] system Library
=================================
   
__[Elo][1] rating system__ is a method for calculating the relative skill levels of players in such competitor-versus-competitor. Mainly used with chess tournaments, tenis and so on.

##Library

There are Factories for most used by USCF and FIDE implementations of system.
These systems use different KFactor. USCF's KFactor is based on current rating, FIDE's kfactor depends of number of plays played.

##Usage

###Model

First you shold define entity that implements `RankedPlayer` interface, and implements all methods.

Next you should use predefined or write you own `KFactorFactory`. This interface compute current kfactor based on current results.

###Duel

If you have models and kfactorFactory you can compute new rating. Just wrap model and points in the game into `PlayerScore`

Next add scores into `Duel` object. After this, you can retrieve new ratings from model.

###Example

    RankedPlayer playerA = new EloModel();
    RankedPlayer playerB = new EloModel();

    PlayerScore scoreA = new PlayerScore(playerA, 3); // 3 points in match
    PlayerScore scoreB = new PlayerScore(playerB, 2); // 2 points in match
    
	Duel duel = new Duel(scoreA, scoreB);
	
    int newRankA = duel.getNewRankA();
    int newRankB = duel.getNewRankB();
    
###Multiplayer games

You can use `MultiPlayerGame` in place of `Duel` to compute rating after match for more players than 2. After multiplayer match new ratings is computed for every pair in the match. For computation is used statistics before match.

















[1]: http://en.wikipedia.org/wiki/Elo_rating_system "ELO"