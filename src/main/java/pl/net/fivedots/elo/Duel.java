package pl.net.fivedots.elo;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 14:16
 */
public class Duel {

    private PlayerScore scoreA;

    private PlayerScore scoreB;

    private ScoreType scoreType;

    public Duel(PlayerScore first, PlayerScore second) {
        this.scoreA = first;
        this.scoreB = second;
        this.scoreType = ScoreType.computeScore(first.getScore(), second.getScore());
        compute();
    }

    public RankedPlayer getPlayerA() {
        return scoreA.getPlayer();
    }

    public RankedPlayer getPlayerB() {
        return scoreB.getPlayer();
    }

    public ScoreType getScoreType() {
        return scoreType;
    }

    public void setScoreType(ScoreType scoreType) {
        this.scoreType = scoreType;
    }

    public int getNewRankA() {
        return scoreA.getNewRank();
    }

    public int getNewRankB() {
        return scoreB.getNewRank();
    }

    public void compute() {
        scoreA.updateRank(Elo.computeChange(getPlayerA(), scoreType, getPlayerB()));
        scoreB.updateRank(Elo.computeChange(getPlayerB(), scoreType.reverse(), getPlayerA()));
    }

}
