package pl.net.fivedots.elo;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 13:55
 */
public class PlayerScore {
    private RankedPlayer player;

    private int score = 0;

    private int newRank = 0;

    public PlayerScore() {
    }

    public PlayerScore(RankedPlayer player) {
        this.player = player;
    }

    public PlayerScore(RankedPlayer player, int score) {
        this.player = player;
        this.score = score;
    }

    public int getNewRank() {
        if (newRank == 0) {
            newRank = player.getCurrentRank();
        }
        return newRank;
    }

    public void setNewRank(int newRank) {
        this.newRank = newRank;
    }

    public RankedPlayer getPlayer() {
        return player;
    }

    public void setPlayer(RankedPlayer player) {
        this.player = player;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void updateRank(double change) {
        newRank = (int) (getNewRank() + Math.round(change));
    }
}
