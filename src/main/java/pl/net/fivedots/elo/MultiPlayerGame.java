package pl.net.fivedots.elo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 16:43
 */
public class MultiPlayerGame {

    private List<PlayerScore> scores;

    public MultiPlayerGame() {
        scores = new ArrayList<PlayerScore>();
    }

    public MultiPlayerGame(List<PlayerScore> scores) {
        this.scores = scores;
    }

    public boolean addScore(PlayerScore score) {
        return this.scores.add(score);
    }

    public List<PlayerScore> newRanks() {
        compute();
        return scores;
    }

    private void compute() {
        if (this.scores.size() < 2) {
            throw new IllegalArgumentException("Should be at least two scores");
        }

        for (PlayerScore ps : scores) {
            ps.setNewRank(ps.getPlayer().getCurrentRank());
        }

        for (int i = 0; i < scores.size(); i++) {
            double ch = 0.0d;
            for (int j = 0; j < scores.size(); j++) {
                if (i != j) {

                    PlayerScore first = scores.get(i);
                    PlayerScore second = scores.get(j);
                    ScoreType scoreType = ScoreType.computeScore(first.getScore(), second.getScore());
                    double change = Elo.computeChange(first.getPlayer(), scoreType, second.getPlayer());
                    ch += change;
                }
            }
            scores.get(i).updateRank(ch);
        }
    }


}
