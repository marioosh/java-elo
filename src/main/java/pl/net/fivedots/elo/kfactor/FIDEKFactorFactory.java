package pl.net.fivedots.elo.kfactor;

import pl.net.fivedots.elo.RankedPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 13:04
 */
public class FIDEKFactorFactory implements KFactorFactory {

    @Override
    public int compute(RankedPlayer player) {
        switch (player.getPlayerType()) {
            case BEGINNER:
                return 30;
            case PRO:
                return 10;
            default:
                return 15;
        }
    }

}
