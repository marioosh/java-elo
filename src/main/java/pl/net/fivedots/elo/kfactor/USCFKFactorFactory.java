package pl.net.fivedots.elo.kfactor;

import pl.net.fivedots.elo.RankedPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 13:05
 */
public class USCFKFactorFactory implements KFactorFactory {
    @Override
    public int compute(RankedPlayer player) {
        int rank = player.getCurrentRank();
        if (rank < 2100) {
            return 32;
        } else if (rank > 2400) {
            return 16;
        } else
            return 24;
    }
}
