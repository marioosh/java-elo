package pl.net.fivedots.elo.kfactor;

import pl.net.fivedots.elo.RankedPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 13:03
 */
public interface KFactorFactory {
    /**
     * @param player currentPlayer
     * @return computed kFactor
     */
    public int compute(RankedPlayer player);
}
