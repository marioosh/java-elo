package pl.net.fivedots.elo.playertype;

import pl.net.fivedots.elo.RankedPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 12:58
 */
public interface PlayerTypeFactory {

    public PlayerType getPlayerType(RankedPlayer player);


}
