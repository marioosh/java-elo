package pl.net.fivedots.elo.playertype;

import pl.net.fivedots.elo.RankedPlayer;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 13:00
 */
public class DefaultPlayerTypeFactory implements PlayerTypeFactory {

    @Override
    public PlayerType getPlayerType(RankedPlayer player) {
        if (player.getPlayedGames() >= 30) {
            if (player.getCurrentRank() >= 2400) {
                return PlayerType.PRO;
            } else {
                return PlayerType.COMMON;
            }

        } else {
            return PlayerType.BEGINNER;
        }
    }
}
