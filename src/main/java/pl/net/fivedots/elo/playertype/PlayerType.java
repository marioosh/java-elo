package pl.net.fivedots.elo.playertype;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 12:36
 */
public enum PlayerType {
    BEGINNER,
    COMMON,
    PRO
}
