package pl.net.fivedots.elo;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 13:58
 */
public enum ScoreType {
    WIN(1.0d),
    DRAW(0.5d),
    LOST(0.0d);

    private double score = 0.0d;

    ScoreType(double score) {
        this.score = score;
    }

    public static ScoreType computeScore(int resultA, int resultB) {
        return resultA > resultB ? WIN : resultA == resultB ? DRAW : LOST;
    }

    public double getScore() {
        return score;
    }

    public ScoreType reverse() {
        switch (this) {
            case DRAW:
                return DRAW;
            case WIN:
                return LOST;
            case LOST:
                return WIN;
            default:
                throw new IllegalArgumentException("Cannot reverse this value");
        }
    }
}
