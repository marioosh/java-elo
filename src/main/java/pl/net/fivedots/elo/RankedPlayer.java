package pl.net.fivedots.elo;

import pl.net.fivedots.elo.playertype.PlayerType;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 12:35
 */
public interface RankedPlayer {


    /**
     * @return PlayerType
     *         Player id marked as Begginer, Common or Pro. This title is based on current rating or
     *         number of plays currently played. Use PlayerTypeFactory if you want to use your own computation.
     */
    public PlayerType getPlayerType();

    /**
     * @return number of games currently played
     */
    public int getPlayedGames();

    /**
     * @return current Rank
     */
    public int getCurrentRank();

    /**
     * @return kFactor for this player. kFactor is computer different in FIDE and USCF systems.
     */
    public int kFactor();

}
