package pl.net.fivedots.elo;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 16:36
 */
public class Elo {

    public static double computeChange(RankedPlayer playerA, ScoreType scoreType, RankedPlayer playerB) {

        double expected = computeExpected(playerA.getCurrentRank(), playerB.getCurrentRank());

        return playerA.kFactor() * (scoreType.getScore() - expected);
    }

    public static double computeExpected(double ratingA, double ratingB) {

        return 1.0 / (1.0 + Math.pow(10, (ratingB - ratingA) / 400));

    }
}
