package pl.net.fivedots.elo;

import pl.net.fivedots.elo.kfactor.FIDEKFactorFactory;
import pl.net.fivedots.elo.kfactor.KFactorFactory;
import pl.net.fivedots.elo.playertype.DefaultPlayerTypeFactory;
import pl.net.fivedots.elo.playertype.PlayerType;
import pl.net.fivedots.elo.playertype.PlayerTypeFactory;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 12:47
 */
public class EloModel implements RankedPlayer {
    public int playedGames = 0;

    public int currentRank = 1400;

    public KFactorFactory kFactorFactory = new FIDEKFactorFactory();

    public PlayerTypeFactory playerTypeFactory = new DefaultPlayerTypeFactory();


    public EloModel() {
    }


    public EloModel(int playedGames, int currentRank) {
        this.playedGames = playedGames;
        this.currentRank = currentRank;
    }

    @Override
    public PlayerType getPlayerType() {
        return playerTypeFactory.getPlayerType(this);
    }

    @Override
    public int getPlayedGames() {
        return this.playedGames;
    }

    @Override
    public int getCurrentRank() {
        return this.currentRank;
    }

    @Override
    public int kFactor() {
        return kFactorFactory.compute(this);
    }

}
