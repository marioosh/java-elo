package pl.net.fivedots.elo;

import pl.net.fivedots.elo.kfactor.KFactorFactory;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 09.10.2012
 * Time: 15:01
 */
public class TestKFactor implements KFactorFactory {
    private int factor;

    public TestKFactor(int factor) {
        this.factor = factor;
    }

    @Override
    public int compute(RankedPlayer player) {
        return factor;
    }
}
