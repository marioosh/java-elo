package pl.net.fivedots.elo;

import org.junit.Test;
import pl.net.fivedots.elo.kfactor.USCFKFactorFactory;
import pl.net.fivedots.elo.playertype.PlayerType;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ELOTest {

    @Test
    public void aVeryImportantThingToTest() {
        assertEquals(2, 1 + 1);
    }

    @Test
    public void gettersDefault() {
        EloModel model = new EloModel();
        assertEquals(0, model.getPlayedGames());

        assertEquals(1400, model.getCurrentRank());
    }

    @Test
    public void getters() {
        EloModel model = new EloModel(12, 1200);
        assertEquals(12, model.getPlayedGames());

        assertEquals(1200, model.getCurrentRank());

    }

    @Test
    public void rank() {
        EloModel model = new EloModel();

        assertEquals(PlayerType.BEGINNER, model.getPlayerType());
        model.playedGames = 29;
        assertEquals(PlayerType.BEGINNER, model.getPlayerType());
        model.playedGames = 30;
        assertEquals(PlayerType.COMMON, model.getPlayerType());

        model.currentRank = 2400;
        assertEquals(PlayerType.PRO, model.getPlayerType());


    }

    //FIDE kFactorTest

    @Test
    public void fideKfactor() {
        EloModel model = new EloModel();
        //beginner
        model.playedGames = 29;

        assertEquals(30, model.kFactor());

        model.playedGames = 30;
        assertEquals(15, model.kFactor());

        model.currentRank = 2400;
        assertEquals(10, model.kFactor());
    }

    @Test
    public void ucsfKFactor() {
        EloModel model = new EloModel();
        model.kFactorFactory = new USCFKFactorFactory();

        assertEquals(32, model.kFactor());

        model.currentRank = 2099;
        assertEquals(32, model.kFactor());

        model.currentRank = 2100;
        assertEquals(24, model.kFactor());

        model.currentRank = 2400;
        assertEquals(24, model.kFactor());

        model.currentRank = 2401;
        assertEquals(16, model.kFactor());
    }

    @Test
    public void computeExpected() {

        assertEquals(0.64, Elo.computeExpected(2000, 1900), 0.05);

    }

    @Test
    public void scoreTestForFactor30() {
        //30 kFactorFactory - beginers
        EloModel lower = new EloModel();
        EloModel higher = new EloModel();

        lower.kFactorFactory = new TestKFactor(30);
        higher.kFactorFactory = new TestKFactor(30);

        lower.currentRank = 1500;
        higher.currentRank = 1600;

        //lower win with higher
        PlayerScore lScore = new PlayerScore(lower, 3);
        PlayerScore hScore = new PlayerScore(higher, 2);

        Duel duel = new Duel(lScore, hScore);

        assertEquals(1519, duel.getNewRankA());
        assertEquals(1581, duel.getNewRankB());

        //draw

        lScore = new PlayerScore(lower, 3);
        hScore = new PlayerScore(higher, 3);

        duel = new Duel(lScore, hScore);
        assertEquals(1504, duel.getNewRankA());
        assertEquals(1596, duel.getNewRankB());

        //lower lost with higher

        lScore = new PlayerScore(lower, 2);
        hScore = new PlayerScore(higher, 3);

        duel = new Duel(lScore, hScore);
        assertEquals(1489, duel.getNewRankA());
        assertEquals(1611, duel.getNewRankB());

    }

    @Test
    public void scoreTestForFactor10() {
        //30 kFactorFactory - beginers
        EloModel lower = new EloModel(0, 1500);
        EloModel higher = new EloModel(0, 1600);

        lower.kFactorFactory = new TestKFactor(10);
        higher.kFactorFactory = new TestKFactor(10);

        //lower win with higher
        PlayerScore lScore = new PlayerScore(lower, 3);
        PlayerScore hScore = new PlayerScore(higher, 2);

        Duel duel = new Duel(lScore, hScore);

        assertEquals(1506, duel.getNewRankA());
        assertEquals(1594, duel.getNewRankB());

        //draw

        lScore = new PlayerScore(lower, 3);
        hScore = new PlayerScore(higher, 3);

        duel = new Duel(lScore, hScore);
        assertEquals(1501, duel.getNewRankA());
        assertEquals(1599, duel.getNewRankB());

        //lower lost with higher

        lScore = new PlayerScore(lower, 2);
        hScore = new PlayerScore(higher, 3);

        duel = new Duel(lScore, hScore);
        assertEquals(1496, duel.getNewRankA());
        assertEquals(1604, duel.getNewRankB());

    }

    @Test
    public void multiPlayerGame() {
        MultiPlayerGame mg = new MultiPlayerGame();

        EloModel lower = new EloModel(0, 1300);
        EloModel mid = new EloModel(0, 1350);
        EloModel higher = new EloModel(0, 1400);
        EloModel highest = new EloModel(0, 1500);

        lower.kFactorFactory = new TestKFactor(15);
        mid.kFactorFactory = new TestKFactor(15);
        higher.kFactorFactory = new TestKFactor(15);
        highest.kFactorFactory = new TestKFactor(15);


        // Lets test....
        mg.addScore(new PlayerScore(lower, 4));
        mg.addScore(new PlayerScore(mid, 3));
        mg.addScore(new PlayerScore(higher, 5));
        mg.addScore(new PlayerScore(highest, 4));

        List<PlayerScore> results = mg.newRanks();

        assertEquals(1300, results.get(0).getPlayer().getCurrentRank());
        assertEquals(1350, results.get(1).getPlayer().getCurrentRank());
        assertEquals(1400, results.get(2).getPlayer().getCurrentRank());
        assertEquals(1500, results.get(3).getPlayer().getCurrentRank());

        assertEquals(1307, results.get(0).getNewRank());
        assertEquals(1331, results.get(1).getNewRank());
        assertEquals(1421, results.get(2).getNewRank());
        assertEquals(1491, results.get(3).getNewRank());
    }

}
